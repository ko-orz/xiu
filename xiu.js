// window.onload = function () {
//     var h1 = this.document.createElement('h1');
//     window.document.body.appendChild(h1);
//     h1.style.color = 'white'
//     setInterval(()=>{
//         h1.innerText = new Date().toLocaleString();
//     })
//     var xiu = new Xiu();
//     var msg = '123421341234';
//     xiu.addXiu(msg, 3);
//     xiu.playXius();
//     window.onclick = function() {
//         xiu.addXiu(msg, 5);
//         xiu.playXius(4);
//     }
// }

function Xiu(config={}) {
    const DEFAULT_KEEP_SEC = config.keepSec || 10;
    const DEFAULT_KEEP_FREQ = config.keepFreq || 20;
    const XIUS_ROUND_WIDTH = config.xiusRoundWidth || screen.width;
    const DEFAULT_KEEP_STEP = XIUS_ROUND_WIDTH / (DEFAULT_KEEP_FREQ * DEFAULT_KEEP_SEC);
    const DEFAULT_INTERVAL_TIME = 1000 / DEFAULT_KEEP_FREQ;
    this.xius = [];
    this.timer;
    that = this;
    this.addXiu = function (msg = '', playTime = 0, extras = {}) {
        var i = 0;
        var xius = that.xius;
        var len = xius.length;
        var xiusIndex = 0;
        for (; i < len; i++) {
            if (xius[i] > playTime) {
                xiusIndex = i;
                break;
            }
        }
        xius.splice(i, 0, {
            msg,
            playTime,
            extras
        })
    }
    this.playXius = function (playTime = 0) {
        clearTimeout(that.timer);
        let startIndex = 0;
        let waitTime = 0;
        for (var i = 0; i < that.xius.length; i++) {
            if (playTime < that.xius[i].playTime) {
                startIndex = i;
                waitTime = that.xius[i].playTime - playTime;
                break;
            } 
        }
        let firstWaitTimer = setTimeout(function(){
            that.goXiu(startIndex);
        }, waitTime * 1000)
    }
    this.goXiu = function (index) {
        clearTimeout(that.timer);
        if (that.xius[index]) {
            that.timer = setTimeout(function () {
                that.playActive(that.xius[index].msg, that.xius[index].playTime, that.xius[index].extras)
                that.goXiu(index + 1);
            }, calculateTime(index)*1000)
        }
        function calculateTime(index) {
            var lastTime = that.xius[index - 1] ? that.xius[index - 1].playTime : 0;
            var thisTime = that.xius[index].playTime;
            return thisTime - lastTime;
        }
    }
    this.playActive = function (msg, playTime, extras) {
        var xiuPunch = that.newXiuPunch(msg, playTime, extras);
        let speed = extras.speed || 1;
        that.speak(msg);
        var intervaler = setInterval(function () {
            let nextRight = parseInt(xiuPunch.style.right) + DEFAULT_KEEP_STEP;
            xiuPunch.style.right = nextRight + 'px';
            if (nextRight > XIUS_ROUND_WIDTH * 1.5) {
                clearInterval(intervaler);
                window.document.body.removeChild(xiuPunch);
            }
        }, DEFAULT_INTERVAL_TIME/speed)
    }
    this.newXiuPunch = function (msg, playTime, extras) {
        let xiuPunch = document.createElement('span');
        if (Object.assign) {
            Object.assign(xiuPunch.style, extras);
        }
        console.log(xiuPunch.style.fontSize);
        xiuPunch.innerText = msg;
        xiuPunch.style.position = 'absolute';
        xiuPunch.style.right = extras.right || 0;
        xiuPunch.style.top = extras.top || Math.random() * XIUS_ROUND_WIDTH | 0;
        xiuPunch.style.color = extras.color || 'white';
        window.document.body.appendChild(xiuPunch);
        return xiuPunch;
    }
    this.speak = function speak(textToSpeak) { 
		var u = new SpeechSynthesisUtterance();
		u.text = textToSpeak; 
 		//汉语
		u.lang = 'zh-CN'; 
 		//日语
 		//u.lang = 'ja-JP';
  		u.rate =1;  
  		speechSynthesis.speak(u);
	} 
}